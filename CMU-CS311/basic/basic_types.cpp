/*
 * basic_types.cpp
 *
 *  Created on: May 14, 2018
 *      Author: hiepnh
 */

#include <iostream>
#include <string>
#include <codecvt>
#include <locale.h>
#include <assert.h>

using namespace std;

void test_integer(){
	unsigned int a = 5;
	unsigned int b = 7;
	unsigned int c = a - b;

	cout<<"c = "<<c<<endl;

}

void test_string(){
	string a = "Hel" "lo";	// auto concat
	cout<<a<<" a.size = "<<a.size()<<endl;

//	wstring_convert<codecvt_utf8_utf16<char16_t>,char16_t> convert; // ... filled in with a codecvt to do UTF-8 <-> UTF-16
//	string utf8_string = u8"This string has UTF-8 content";
//	u16string utf16_string = convert.from_bytes(utf8_string);
//	string another_utf8_string = convert.to_bytes(utf16_string);
}

void test_reference(){
	int x;
	int& y = x; // y is lvalue reference to int
	int&& tmp = 3; // tmp is rvalue reference to int

	int i = 42;
	int& j = i;
	j = 41;
	assert(j == 42);
	cout<<j<<endl;
	cout<<i<<endl;
}

///////////////////
int main(int argc, char* args[]) {

	//
	test_integer();
	test_string();
	test_reference();

	//
	return 0;
}


