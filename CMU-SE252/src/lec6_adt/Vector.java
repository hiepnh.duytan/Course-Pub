package lec6_adt;

public class Vector {
	public double ix;
	public double iy;
	public double tx;
	public double ty;
	
	//
	public Vector(){
		this.ix = 0;
		this.iy = 0;
		this.tx = 0;
		this.ty = 0;
	}
	
	public Vector(double ix, double iy, double tx, double ty) {
		super();
		this.ix = ix;
		this.iy = iy;
		this.tx = tx;
		this.ty = ty;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Vector))
			return false;
		else{
			Vector temp = (Vector)obj;
			return (temp.tx - temp.ix == this.tx - this.ix) && (temp.ty - temp.iy == this.ty - this.iy);
		}
	}
	
	//
//	public Vector add(Vector a, Vector b){
//		return new Vector(a.ix + b.ix, );
//	}
	
	
}
