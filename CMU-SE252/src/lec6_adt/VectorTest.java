package lec6_adt;

public class VectorTest {

	public static void main(String[] args) {
		Vector a = new Vector(1,2,3,4);
		Vector b = new Vector(5,6,7,8);

		System.out.println(a == b);
		System.out.println(a.equals(b));
	}

}
